import torch
import torch.nn as nn
import torch.nn.functional as F

from collections import OrderedDict


in_channel = 1
out_channel = 20
# Single sequential model
layers = OrderedDict([
    ('conv1', nn.Conv1d(in_channel, out_channel, groups=1, bias=True, kernel_size=2, padding=0, stride=1)), 
    ('relu1'), F.relu(),
    ('conv2', nn.Conv1d(20, 64, groups=1, bias=True, kernel_size=5, padding=0, stride=1)),
    ('relu2', F.relu())
])

model = nn.Sequential(layers)

print(model)