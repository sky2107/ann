from __future__ import print_function
import torch

empty = torch.empty(5,3, dtype=torch.int)
zeros = torch.zeros(5,3, dtype=torch.double)
rand = torch.rand(5,3)

print(empty)
print(rand)
print(zeros)

