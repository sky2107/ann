# autograd explained 
import torch

a = torch.tensor(2.0, requires_grad=True)
b = torch.tensor(3.0, requires_grad=True)

c = a * b

# information about tensors 
def info_about_parameters(para):
    print(para.grad)
    print(para.grad_fn)
    print(para.is_leaf)
    print(para.requires_grad)
    print('------------------------------------------')

info_about_parameters(c)

d = torch.tensor(4.0, requires_grad=True)

e = c * d

info_about_parameters(e)

# pointer arithmetic 

e.backward()

info_about_parameters(a)