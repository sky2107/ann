from PIL import Image
import sys



def info(path: str):
    try:
        im = Image.open(path)

    except IOError:
        print("Unable to load image")
        sys.exit(1)
        
    print("Format: {0}\nSize: {1}\nMode: {2}".format(im.format, 
        im.size, im.mode))

if __name__ == '__main__':
    path = '../../data/screenshots/'
    gray_path = '../../data/screenshots/gray_img/'
    blurred_path = '../../data/screenshots/blurred_img/'

    info(path + '1.jpg')
    info(blurred_path + '1.jpg')
    info(gray_path + '1.jpg')