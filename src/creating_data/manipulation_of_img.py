#!/usr/bin/python3

from PIL import Image, ImageFilter
import sys
import os

class Manipulator:
    '''
        For the manipulation of the images
        to create more content

        TODO maybe Static parameter or methods
    '''
    def __init__(self, flag: bool = False):
        # TODO os.join or constant path names
        self.path = '../../data/screenshots/'
        self.gray_path = '../../data/screenshots/gray_img/'
        self.blurred_path = '../../data/screenshots/blurred_img/'
        # after the last folder 5 folder inthe dir named level_ + rotation_level
        self.rotation_path = '../../data/screenshots/rotation_img/'
        self.mirroring_path = '../../data/screenshots/mirroring_img/'
        # center 10 - 20 percent
        self.crop_path = '../../data/screenshots/crop_img/'
        # endings
        self.jpg = '.jpg'
        self.png = '.png'

        # getting all images from cal
        self.listOfImages = self.getAllImages(self.path, self.jpg)

        # single image plus filename
        self.img = None
        self.filename = None

        if flag:
            self.open_image(self.listOfImages[6])

        # rotation angels 5 levels 0 - 4
        self.rotation_angles = [15, 30, 45, 90, 180]

        # cropping generalizable 10 - 20 percent
        self.crop_reduction_10_percent = None
        self.crop_reduction_20_percent = None

    def info(self):
        im = self.img
        print("Format: {0}\nSize: {1}\nMode: {2}".format(im.format, 
        im.size, im.mode))

    def getAllImages(self, path: str, ending: str) -> list:
        list_of_files = []
        for file in os.listdir(path):
            if file.endswith(ending):
                list_of_files.append(os.path.join(path, file))
        
        return list_of_files

    def open_image(self, name: str):
        try:
            self.img = Image.open(name)
            self.getFilename()
    
        except IOError:
            print("Unable to load image")    
            sys.exit(1)

    def saveImage(self, img, path):
        '''
            Try Catch TODO
        '''
        img.save(path)

    def getFilename(self):
        self.filename = self.img.filename.split(self.path)[1]
        # print(self.filename)

    def show(self, img = None):
        # print(img)
        if img == None:
            img = self.img
        img.show()
    
    def createFolderStructure(self, dirName):
        # TODO 
        os.mkdir(dirName)
    # -------------------------------------------------------------------------------
    # BLURRED

    def blurrAllImages(self):
        for file in self.listOfImages:
            self.open_image(file)
            self.blur_image()

    def blur_image(self, name_of_file: str = ''):
        # name_of_file_tuple = os.path.splitext(self.img.filename)
        if name_of_file == '':
            name_of_file = self.img.filename.split(self.path)[1]
        blurred = self.img.filter(ImageFilter.BLUR)
        folder_blurred = 'blurred_img'
        step_1 = os.path.join(self.path, folder_blurred)
        final_step = os.path.join(step_1, name_of_file)
        self.saveImage(blurred,final_step)

    # -------------------------------------------------------------------------------

    def grayAllImages(self):
        for file in self.listOfImages:
            self.open_image(file)
            self.convertImgToGray()

    def convertImgToGray(self, img = None):
        if img == None:
            img = self.img
        img = img.convert('L')
        final_path = os.path.join(self.gray_path, self.filename)
        self.saveImage(img, final_path)

    # -------------------------------------------------------------------------------
    # ROTATION

    def rotationOfAllElements(self):
        for file in self.listOfImages:
            self.open_image(file)
            for count, angle in enumerate(self.rotation_angles):
                # [15, 30, 45, 90, 180]
                # print(count)
                # print(angle)
                self.rotation(self.img, angle, count + 1)
            # break
    
    def rotation(self, img, angle: int, position):
        rotated = img.rotate(angle)
        final_path = self.rotation_path + 'level_' + str(position)
        rotated.save(os.path.join(final_path, self.filename))

    # -------------------------------------------------------------------------------
    # MIRROR

    def mirrorAllImages(self):
        for file in self.listOfImages:
            self.open_image(file)
            self.mirroringOfImage(self.img)

    def mirroringOfImage(self, img):
        image_obj = img
        # transpose flips the pixels
        rotated_image = image_obj.transpose(Image.FLIP_LEFT_RIGHT)
        rotated_image.save(os.path.join(self.mirroring_path, self.filename))
    
    # -------------------------------------------------------------------------------
    # CROP

    def cropAllImages(self):
        for file in self.listOfImages:
            self.open_image(file)
            self.croppingImage(self.img)
            self.croppingImage(self.img, False)
            self.croppingImage(self.img, True, 'left')
            self.croppingImage(self.img, False,'left')
            self.croppingImage(self.img, True, 'right')
            self.croppingImage(self.img, False,'right')

    def croppingImage(self, img, flag: bool = True, method='center'):
        '''
            center 10 to 20 percent
            flag default to folder center_10
        '''
        # print(img.size)
        w, h = img.size
        square = False
        if w == h:
            reduction = self.cropGeneralizableSquare(w, flag)
            # TODO better architecture
            reduction_for_w = reduction
            reduction_for_h = reduction_for_w
            square = True
        else:
            # TODO better architecture
            reduction_for_w = self.cropGeneralizableSquare(w, flag)
            reduction_for_h = self.cropGeneralizableSquare(h, flag)
            # self.cropGeneralizable()
        
        # with Flag
        # The crop() method takes a 4-tuple defining 
        # the left, upper, right, and lower pixel coordinates.
        # Unnecessary TODO cleaning up 
        if square and method == 'center':
            left  = 0 + reduction
            upper = 0 + reduction
            lower = w - reduction
            right = w - reduction
        else:
            # upper corner and lower corner will not be modified
            if method == 'center':
                left  = 0 + reduction_for_w
                upper = 0 + reduction_for_h
                lower = w - reduction_for_h
                right = w - reduction_for_w
            else:
                # constant values TODO ???
                upper = 0
                lower = h
            if method == 'left':
                left  = 0 + reduction_for_w
                right = w
            elif method == 'right':
                left = 0
                right = w - reduction_for_w
        
        # print(reduction)
        croppi_img = img.crop((left, upper, lower, right))
        # croppi_img.show()

        if method == 'center':
            if flag:
                path = os.path.join(self.crop_path, 'center_10/')
            else: 
                path = os.path.join(self.crop_path, 'center_20/')
        elif method == 'left':
            if flag:
                path = os.path.join(self.crop_path, 'left_10/')
            else: 
                path = os.path.join(self.crop_path, 'left_20/')
        else: 
            if flag:
                path = os.path.join(self.crop_path, 'right_10/')
            else: 
                path = os.path.join(self.crop_path, 'right_20/')

        croppi_img.save(os.path.join(path, self.filename))
    
    def cropGeneralizableSquare(self, a: int, flag_10_percent: bool=True):
        self.crop_reduction_10_percent = a / 10
        self.crop_reduction_20_percent = a / 5

        # int division
        reduction = a // 10 if flag_10_percent else a // 5

        return reduction

    # -------------------------------------------------------------------------------

    def creatAllManipulations(self):
        self.blurrAllImages()
        self.grayAllImages()
        self.rotationOfAllElements()
        self.mirrorAllImages()
        self.cropAllImages()
        
if __name__ == '__main__':

    mani = Manipulator()

    