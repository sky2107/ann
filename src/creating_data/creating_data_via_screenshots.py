import pyautogui
from PIL import Image

def press(button: str):
    pyautogui.press(button)

def enter(text: str, interval: float = .1):
    pyautogui.typewrite(text, interval=interval)

calculator_w = 30
calculator_h = 30
# type is int because it is about pixel
def screenshots(name: str, starting_pos_x: int, starting_pos_y: int, width: int = calculator_w, height: int = calculator_h, typeOfimg: bool = True):
    # starting_pos_x, starting_pos_y, width, height
    path = '../../data/screenshots/'
    img_type = ".jpg" if typeOfimg else ".png"
    # print(img_type)
    concat_name = '' + path + name + img_type
    # save autom. 
    im = pyautogui.screenshot(concat_name, region=(starting_pos_x, starting_pos_y, width, height))

def creatingSimpleSampleOfImages(quantity: int) -> list:
    array_of_images = None
    for _ in range(quantity):
        im = Image.open('image.jpg')

def getCurrentPosition():
    screenWidth, screenHeight = pyautogui.size()
    currentMouseX, currentMouseY = pyautogui.position()
    print(currentMouseX, currentMouseY)

    return currentMouseX, currentMouseY

def calculator_im_zeroToNine():
   
    # careful with this images
    pos_cal_x , pos_cal_y = (1424, 188)
    # click on calcuulator
    pyautogui.click(pos_cal_x , pos_cal_y)
    for i in range(10):
        enter(str(i))
        screenshots(str(i), pos_cal_x , pos_cal_y)
        press('del')
        

if __name__ == '__main__':
    
    calculator_im_zeroToNine()