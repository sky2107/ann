from PIL import Image

path = '../../data/screenshots/'
gray_path = '../../data/screenshots/gray_img/'
blurred_path = '../../data/screenshots/blurred_img/'
 
def flip_image(image_path, saved_location):
    """
    Flip or mirror the image
 
    @param image_path: The path to the image to edit
    @param saved_location: Path to save the cropped image
    """
    image_obj = Image.open(image_path)
    # matrix formed from another matrix by interchanging the rows and columns
    # for further details https://pillow.readthedocs.io/en/3.1.x/reference/Image.html
    rotated_image = image_obj.transpose(Image.FLIP_TOP_BOTTOM)
    rotated_image.save(saved_location)
    rotated_image.show()
 
if __name__ == '__main__':
    image = '../../data/screenshots/1.jpg'
    flip_image(image, 'flipped_1.jpg')