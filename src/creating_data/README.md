# Implementation journey

VIA screenshot generating data
TODO implementing creating_data_via_screenshots.py
    with manipulation class

ONLY JPG format at first
8th TODO 

> 1st screenshots via calculator of win10 <br>
> creating 10 imgs <br>
> 2nd image details cal related via <br>

- Format: JPEG
- Size: (30, 30)
- Mode: RGB

> 3rd manipulation <br>
> all images are blurred via manipulation class <br>
> 4th convert to gray => color channels <br>
> 5th rotation 5 level <br> 

- 15 30 45 90 180

> 6th mirroring all images in other words flipping the image <br>
> 7th cropping <br>
> 3 ways and difficulty must be generalizable <br>
> Left Right Center cropping <br>
> 10 - 20 percent reduction <br>

> 8th creating folder structure automatically

#

> Short history about the program

1. creating_data_via_screenshot -> data cal 
2. info_img.py
3. manipulation_of_img.py => mani = Manipulator() => mani.blurrAllImages()
4. convert to gray the data => mani.grayAllImages()
5. rotation of the image => mani.rotationOfAllElements()
6. mirroring => mani.mirrorAllImages()
7. crop => mani.cropAllImages() left, right and center with 10 to 20 reduction
8. os.mkdir(dirName)


# 

> Path information

    current path for anaconda into the shell
    conda activate rpa
    cd Documents\ann\src\creating_data
