import torch
import torch.nn as nn
import torch.nn.functional as F

import torch.autograd.variable as Variable

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()

        self.conv1 = nn.Conv2d(2, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)

        


    def forward(self, x):
        x = F.relu(self.conv1(x))
        # x = F.max_pool2d(x, (3))
        
        
        return x

if __name__ == "__main__":
    net = Net()

    input = torch.randn(32, 2, 6, 6)
    # input = input.unsqueeze(0)
    out = net(input)

    print(out.size())

    size = out.size()[1:]
        
    num_features = 1
    for s in size:
        print(s)
        num_features *= s
    
    print(num_features)